const uuidv4 = require("uuid/v4");

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      { tableName: "property", schema: "properties" },
      [
        {
          id: uuidv4(),
          name: "Appartment 1",
        },
        {
          id: uuidv4(),
          name: "Townhouse 1",
        },
        {
          id: uuidv4(),
          name: "Shooping Center 1",
        },
      ],
      {}
    ),
  down: (queryInterface, Sequelize) =>
    queryInterface.bulkDelete(
      { tableName: "property", schema: "properties" },
      null,
      {}
    ),
};
