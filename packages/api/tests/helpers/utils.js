const Sequelize = require("sequelize");
const sequelize_fixtures = require("sequelize-fixtures");
const models = require("../../src/models/database/models");

module.exports = {
  cleanDatabase: () =>
    Promise.all(
      Object.keys(models).map((key) => {
        if (["sequelize", "Sequelize"].includes(key)) return null;
        return models[key].destroy({ where: {}, force: true });
      })
    ),
  initData: () =>
    sequelize_fixtures
      .loadFile("tests/fixtures/testData.json", models)
      .then(function () {}),
  generateProperties: () => _generateProperties(),
};
