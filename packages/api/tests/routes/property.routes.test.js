const request = require("supertest");
const server = require("../../src/app");
const uuidv4 = require("uuid/v4");
var db = require("../../src/models/database/models");
const testHelpers = require("../helpers/utils");
const models = require("../../src/models/database/models");
const { Property } = models;

describe("Properties", () => {
  beforeAll(async () => {
    await testHelpers.cleanDatabase();
    await testHelpers.initData();
    await db.sequelize.sync();
  });

  describe("/GET properties", () => {
    it("should get all tasks", async (done) => {
      const res = await request(server).get("/properties");
      expect(res.status).toBe(200);
      done();
    });
  });

  describe("/GET property/:id", () => {
    it("should get property with id", async (done) => {
      const id = uuidv4();
      const name = "Test Condo t1";
      const description = "Route tests: Get By Id";
      const property = await Property.create({
        id: id,
        name: name,
        description: description,
      });
      const res = await request(server).get(`/properties/${id}`);
      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("id", id);
      expect(res.body).toHaveProperty("name", name);
      expect(res.body).toHaveProperty("description", description);
      done();
    });
  });

  describe("/GET property/:id", () => {
    it("Get - Bad id", async (done) => {
      const id = "not valid id";
      const res = await request(server).get(`/properties/${id}`);
      expect(res.status).toBe(422);
      done();
    });
  });

  describe("/GET property/:id", () => {
    it("Get - id doesn't exist", async (done) => {
      const id = "04c5fe2a-b3a1-48ec-b712-3960f70c2876";
      const res = await request(server).get(`/properties/${id}`);
      expect(res.status).toBe(200);
      done();
    });
  });

  describe("/DELETE property/:id", () => {
    it("should delete property with id", async (done) => {
      const id = "c62c5e7b-990d-49f5-8e89-7d1d0521fb67";
      const name = "Test Condo t2";
      const description = "Route tests: delete";
      const property = await Property.create({
        id: id,
        name: name,
        description: description,
      });
      const res = await request(server).delete(`/properties/${property.id}`);
      expect(res.status).toBe(204);
      done();
    });
  });

  describe("/POST properties", () => {
    it("should create new task successfuly", async (done) => {
      const body = {
        id: uuidv4(),
        name: "Test Condo t3",
        description: "Route tests: create",
      };
      const res = await request(server).post("/properties").send(body);
      expect(res.status).toBe(201);
      done();
    });

    it("should update task successfuly", async (done) => {
      const id = "cdd2a921-607a-4771-81a2-aacd124be365";
      const name = "Test Condo t4";
      const description = "Route tests: update";
      const property = await Property.create({
        id: id,
        name: name,
        description: description,
      });

      const body = {
        name: "Test Condo t4 - After Update",
        description: "Route tests: update - After Update",
      };
      const res = await request(server)
        .put(`/properties/${property.id}`)
        .send(body);
      expect(res.status).toBe(200);
      done();
    });
  });
});
