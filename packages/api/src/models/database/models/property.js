"use strict";
module.exports = (sequelize, DataTypes) => {
  const Property = sequelize.define(
    "Property",
    {
      id: { type: DataTypes.UUIDV4, primaryKey: true },
      name: DataTypes.STRING,
      description: DataTypes.STRING,
    },
    {
      tableName: "property",
      schema: "properties",
      timestamps: false,
    }
  );
  Property.associate = function (models) {
    // associations can be defined here
  };
  return Property;
};
