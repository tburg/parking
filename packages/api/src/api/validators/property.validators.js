const { check, validationResult } = require("express-validator");

// Create
exports.create = [
  check("id")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage("id is required.")
    .bail()
    .isUUID(4)
    .withMessage("id is not in valid format.")
    .bail(),
  check("name")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage("name is required.")
    .bail()
    .isString()
    .withMessage("name is not in valid format.")
    .bail()
    .isLength({ min: 5, max: 100 })
    .withMessage(
      "name must be at least 5 characters long and no longer than 100is not in valid format."
    )
    .bail(),
  check("description")
    .trim()
    .escape()
    .optional()
    .isLength({ max: 100 })
    .withMessage(
      "name must be at least 5 characters long and no longer than 100is not in valid format."
    )
    .bail(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    next();
  },
];

// Retrieve - All
exports.retrieveAll = [
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    next();
  },
];

// Retrieve - One
exports.retrieveOne = [
  check("id")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage("id is required.")
    .bail()
    .isUUID(4)
    .withMessage("id is not in valid format.")
    .bail(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    next();
  },
];

// Update
exports.update = [
  check("id")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage("id is required.")
    .bail()
    .isUUID(4)
    .withMessage("id is not in valid format.")
    .bail(),
  check("name")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage("name is required.")
    .bail()
    .isString()
    .withMessage("name is not in valid format.")
    .bail()
    .isLength({ min: 5, max: 100 })
    .withMessage(
      "name must be at least 5 characters long and no longer than 100is not in valid format."
    )
    .bail(),
  check("description")
    .trim()
    .escape()
    .optional()
    .isLength({ max: 100 })
    .withMessage(
      "name must be at least 5 characters long and no longer than 100is not in valid format."
    )
    .bail(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    next();
  },
];

// Delete
exports.delete = [
  check("id")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage("id is required.")
    .bail()
    .isUUID(4)
    .withMessage("id is not in valid format.")
    .bail(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    next();
  },
];
