require("dotenv").config();

module.exports = {
  development: {
    username: process.env.SQL_DB_USERNAME,
    password: process.env.SQL_DB_PASSWORD,
    database: process.env.SQL_DB,
    host: process.env.SQL_DB_HOST,
    dialect: "postgres",
    logging: false,
  },
  test: {
    username: process.env.SQL_DB_USERNAME,
    password: process.env.SQL_DB_PASSWORD,
    database: process.env.SQL_DB,
    host: process.env.SQL_DB_HOST,
    dialect: "postgres",
    logging: false,
  },
  production: {
    username: process.env.SQL_DB_USERNAME,
    password: process.env.SQL_DB_PASSWORD,
    database: process.env.SQL_DB,
    host: process.env.SQL_DB_HOST,
    dialect: "postgres",
    logging: false,
  },
};
