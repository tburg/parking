var express = require("express");
var router = express.Router();
const { body, check, validationResult } = require("express-validator");
var propertyController = require("../controllers/property.controller");
var propertyValidators = require("../validators/property.validators");

// Create
router.post("/", propertyValidators.create, propertyController.create);

// Retrieve - All
router.get("/", propertyValidators.retrieveAll, propertyController.retrieveAll);

//Retrieve - One;
router.get(
  "/:id",
  propertyValidators.retrieveOne,
  propertyController.retrieveOne
);

// Update
router.put("/:id", propertyValidators.update, propertyController.update);

// Delete
router.delete("/:id", propertyValidators.delete, propertyController.delete);

module.exports = router;
