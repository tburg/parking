const uuidv4 = require("uuid/v4");
var db = require("../../src/models/database/models");
const testHelpers = require("../helpers/utils");

beforeAll(async () => {
  await testHelpers.cleanDatabase();
  await testHelpers.initData();
  await db.sequelize.sync();
});

test("create property", async () => {
  expect.assertions(1);
  const id = uuidv4();
  const property = await db.Property.create({
    id: id,
    name: "Test Apartments t1",
    description: "Database tests: create property",
  });
  expect(property.id).toEqual(id);
});

test("retrieve - all property", async () => {
  expect.assertions(1);
  const properties = await db.Property.findAll();
  expect(properties.length).toBeGreaterThan(0);
});

test("retrieve - one property", async () => {
  expect.assertions(2);
  const id = uuidv4();
  const name = "Test Apartments t2";
  const description = "Database tests: get property";
  const property = await db.Property.create({
    id: id,
    name: name,
    description: description,
  });
  const propertyGet = await db.Property.findByPk(id);
  expect(propertyGet.name).toEqual(name);
  expect(propertyGet.description).toEqual(description);
});

test("delete property", async () => {
  expect.assertions(1);
  const id = uuidv4();
  const name = "Test Apartments t3";
  const description = "Database tests: delete property";
  const property = await db.Property.create({
    id: id,
    name: name,
    description: description,
  });
  await db.Property.destroy({
    where: {
      id: id,
    },
  });
  const propertyDelete = await db.Property.findByPk(id);
  expect(propertyDelete).toBeNull();
});

afterAll(async () => {
  await db.sequelize.close();
});
