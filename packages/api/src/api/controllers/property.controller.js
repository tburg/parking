var db = require("../../models/database/models");

// Create
exports.create = async (req, res) => {
  // New
  const property = {
    id: req.body.id,
    name: req.body.name,
    description: req.body.description,
  };
  // Save
  db.Property.create(property)
    .then((data) => {
      res.status(201).send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Error occurred while creating property.",
      });
    });
};

// Retrieve - All
exports.retrieveAll = (req, res) => {
  var condition = null; // Helpful for future query
  db.Property.findAll({ where: condition })
    .then((data) => {
      res.status(200).send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Error occurred while retrieving properties.",
      });
    });
};

// Retrieve - One
exports.retrieveOne = (req, res) => {
  const id = req.params.id;
  db.Property.findByPk(id)
    .then((data) => {
      res.status(200).send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error occurred while retrieving property with an id of: ${id}.`,
      });
    });
};

// Update
exports.update = (req, res) => {
  const id = req.params.id;
  db.Property.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.status(200).send({
          message: "Property updated successfully.",
        });
      } else {
        res.status(404).send({
          // Since not creating on update. Could be 200 as well, plush out in testing
          message: `Cannot update property with id of: ${id}. It might not exist or nothing to update was specified.`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error updating property with id of: ${id}.`,
      });
    });
};

// Delete
exports.delete = (req, res) => {
  const id = req.params.id;
  db.Property.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.status(204).send({
          message: "Property deleted successfully.",
        });
      } else {
        res.status(404).send({
          message: `Cannot delete property with id of: ${id}. It might not exist.`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error deleting property with id of: ${id}.`,
      });
    });
};
